package com.shumiq.indoorlocation;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class MapActivity extends AppCompatActivity {

    private EditText etX;
    private EditText etY;
    private Button btSave;
    private TextView tvLocation;
    private RelativeLayout indoorBg;
    private RelativeLayout pointer;
    private APInfo ap1,ap2,ap3,ap4;
    private boolean isInitAp = false;
    private WifiManager wifiManager = null;
    private WifiScanReceiver wifiScanReceiver = null;
    private int dimensionX,dimensionY;
    private int originalX,originalY;
    private long currentTime = 0;
    private String UID;
    private Location expPosition;
    private int count=1;
    private float avgError=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        currentTime = System.currentTimeMillis();
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        indoorBg = (RelativeLayout) findViewById(R.id.rlIndoorBg);
        pointer = (RelativeLayout) findViewById(R.id.rlLocationPointer);
        etX = (EditText) findViewById(R.id.etX);
        etY = (EditText) findViewById(R.id.etY);
        btSave = (Button) findViewById(R.id.btSave);

        expPosition = new Location();

        wifiManager =  (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiScanReceiver = new WifiScanReceiver();
        registerReceiver(wifiScanReceiver,new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifiManager.startScan();
        System.out.println("[IQ] Start Scan");

        FirebaseDatabase.getInstance().getReference("/").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                dimensionX = dataSnapshot.child("MapDimension/x").getValue(Integer.class);
                dimensionY = dataSnapshot.child("MapDimension/y").getValue(Integer.class);
                ViewGroup.LayoutParams lp = indoorBg.getLayoutParams();
                if(!isInitAp){
                    originalX = indoorBg.getWidth();
                    originalY = indoorBg.getHeight();
                }

                expPosition.x = dataSnapshot.child("experiment/position/x").getValue(Integer.class);
                expPosition.y = dataSnapshot.child("experiment/position/y").getValue(Integer.class);
                etX.setText(""+expPosition.x);
                etY.setText(""+expPosition.y);

                DataSnapshot data = dataSnapshot.child("/experiment/p"+(int)expPosition.x+"-"+(int)expPosition.y);
                ArrayList<Location> posData = new ArrayList<>();
                for(DataSnapshot pos : data.getChildren()){
                    Location p = new Location();
                    try {
                        p.x = pos.child("x").getValue(Float.class);
                        p.y = pos.child("y").getValue(Float.class);
                        posData.add(p);
                    }
                    catch (Exception e) {
                        return ;
                    }
                }
                avgError=0;
                for(Location p : posData){
                    avgError += Location.Distance(p,expPosition);
                }
                if(posData.size()>0)avgError/=posData.size();

                //System.out.println("-----");
                //System.out.println("[IQ] " + indoorBg.getWidth() + " " + indoorBg.getHeight());
                //System.out.println("[IQ] " + lp.width + " " + lp.height);
                if((double)originalX/originalY<(double)dimensionX/dimensionY){
                    //Change Height
                    lp.width = -1;
                    lp.height = originalX*dimensionY/dimensionX;
                }
                else {
                    lp.height = -1;
                    lp.width = originalY*dimensionX/dimensionY;
                }
                indoorBg.setLayoutParams(lp);
                //System.out.println("[IQ] " + indoorBg.getWidth() + " " + indoorBg.getHeight());
                //System.out.println("[IQ] " + lp.width + " " + lp.height);

                ap1 = new APInfo(dataSnapshot.child("APInfo/AP1/name").getValue(String.class)
                        ,dataSnapshot.child("DummyData/AP1").getValue(Float.class));
                ap2 = new APInfo(dataSnapshot.child("APInfo/AP2/name").getValue(String.class)
                        ,dataSnapshot.child("DummyData/AP2").getValue(Float.class));
                ap3 = new APInfo(dataSnapshot.child("APInfo/AP3/name").getValue(String.class)
                        ,dataSnapshot.child("DummyData/AP3").getValue(Float.class));
                ap4 = new APInfo(dataSnapshot.child("APInfo/AP4/name").getValue(String.class)
                        ,dataSnapshot.child("DummyData/AP4").getValue(Float.class));
                if(!isInitAp){
                    Toast.makeText(MapActivity.this,"Initialize",Toast.LENGTH_SHORT).show();
                }
                isInitAp=true;
                //wifiManager.startScan(); //For Dummy Test
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float x=Float.parseFloat(""+etX.getText());
                float y=Float.parseFloat(""+etY.getText());
                FirebaseDatabase.getInstance().getReference("experiment/position/x").setValue(x);
                FirebaseDatabase.getInstance().getReference("experiment/position/y").setValue(y);
            }
        });
    }

    private int[] CheckValid(Location current){
        Location p1 = new Location(0f,0f);
        Location p2 = new Location(dimensionX,0f);
        Location p3 = new Location(0f,dimensionY);
        Location p4 = new Location(dimensionX,dimensionY);
        int weight[]= new int[10];
        //*
        switch (AlmostEqual(Location.Distance(p1,current)/Location.Distance(p2,current),Math.pow(10,ap1.getStrength()-ap2.getStrength()))){
            case 0:
                //System.out.println("Middle");
                weight[2]++;
                weight[5]++;
                weight[8]++;
                break;
            case 1:
                //System.out.println("Left");
                weight[1]++;
                weight[4]++;
                weight[7]++;
                break;
            case -1:
                //System.out.println("Right");
                weight[3]++;
                weight[6]++;
                weight[9]++;
                break;
        }

        switch (AlmostEqual(Location.Distance(p1,current)/Location.Distance(p3,current),Math.pow(10,ap1.getStrength()-ap3.getStrength()))){
            case 0:
                //System.out.println("Middle");
                weight[4]++;
                weight[5]++;
                weight[6]++;
                break;
            case 1:
                //System.out.println("Bottom");
                weight[1]++;
                weight[2]++;
                weight[3]++;
                break;
            case -1:
                //System.out.println("Top");
                weight[7]++;
                weight[8]++;
                weight[9]++;
                break;
        }
        //*/

        //*
        switch (AlmostEqual(Location.Distance(p1,current)/Location.Distance(p4,current),Math.pow(10 ,ap1.getStrength()-ap4.getStrength()))){
            case 0:
                //System.out.println("Middle");
                weight[3]++;
                weight[5]++;
                weight[7]++;
                break;
            case 1:
                //System.out.println("BottomLeft");
                weight[1]++;
                weight[2]++;
                weight[4]++;
                break;
            case -1:
                // System.out.println("TopRight");
                weight[6]++;
                weight[8]++;
                weight[9]++;
                break;
        }

        switch (AlmostEqual(Location.Distance(p2,current)/Location.Distance(p3,current),Math.pow(10,ap2.getStrength()-ap3.getStrength()))){
            case 0:
                //System.out.println("Middle");
                weight[1]++;
                weight[5]++;
                weight[9]++;
                break;
            case 1:
                //System.out.println("BottomRight");
                weight[2]++;
                weight[3]++;
                weight[6]++;
                break;
            case -1:
                //System.out.println("TopLeft");
                weight[4]++;
                weight[7]++;
                weight[8]++;
                break;
        }
        //*/
        //*
        switch (AlmostEqual(Location.Distance(p2,current)/Location.Distance(p4,current),Math.pow(10,ap2.getStrength()-ap4.getStrength()))){
            case 0:
                //System.out.println("Middle");
                weight[4]++;
                weight[5]++;
                weight[6]++;
                break;
            case 1:
                //System.out.println("Bottom");
                weight[1]++;
                weight[2]++;
                weight[3]++;
                break;
            case -1:
                //System.out.println("Top");
                weight[7]++;
                weight[8]++;
                weight[9]++;
                break;
        }

        switch (AlmostEqual(Location.Distance(p3,current)/Location.Distance(p4,current),Math.pow(10,ap3.getStrength()-ap4.getStrength()))){
            case 0:
                //System.out.println("Middle");
                weight[2]++;
                weight[5]++;
                weight[8]++;
                break;
            case 1:
                //System.out.println("Left");
                weight[1]++;
                weight[4]++;
                weight[7]++;
                break;
            case -1:
                //System.out.println("Right");
                weight[3]++;
                weight[6]++;
                weight[9]++;
                break;
        }
        //*/
        /*
        System.out.println("------");
        System.out.println(weight[7] + " " + weight[8] + " " + weight[9]);
        System.out.println(weight[4] + " " + weight[5] + " " + weight[6]);
        System.out.println(weight[1] + " " + weight[2] + " " + weight[3]);
        System.out.println("------");
        //*/
        return weight;
    }

    private int AlmostEqual(double a, double b){
        if(Math.abs(a-b)<=0.01)return 0;
        if(a>b)return 1;
        return -1;
    }

    public class WifiScanReceiver extends BroadcastReceiver {

        public ArrayList<ScanResult> wifiList = new ArrayList<>();

        @SuppressLint("UseValueOf")
        public void onReceive(Context c, Intent intent) {
            //Toast.makeText(MapActivity.this,"Update!",Toast.LENGTH_SHORT).show();
            //System.out.println("Receive!!");
            //System.out.println("[IQ] Scan Again in " + ((currentTime+3000) - System.currentTimeMillis()));
            if(System.currentTimeMillis()<currentTime+3000){
                wifiManager.startScan();
                return;
            }
            currentTime = System.currentTimeMillis();
            wifiList = (ArrayList<ScanResult>) wifiManager.getScanResults();
            //System.out.println("[IQ] Found " + wifiList.size() + " Networks");
            int apCount = 0;
            for (ScanResult wifi : wifiList){
                if(isInitAp){
                    //System.out.println(wifi.toString());
                    if(ap1.getName().equals(wifi.SSID)){
                        apCount++;
                        ap1.setStrength(Math.max(1,-wifi.level/20f));
                    }
                    if(ap2.getName().equals(wifi.SSID)){
                        apCount++;
                        ap2.setStrength(Math.max(1,-wifi.level/20f));
                    }
                    if(ap3.getName().equals(wifi.SSID)){
                        apCount++;
                        ap3.setStrength(Math.max(1,-wifi.level/20f));
                    }
                    if(ap4.getName().equals(wifi.SSID)){
                        apCount++;
                        ap4.setStrength(Math.max(1,-wifi.level/20f));
                    }
                }
            }
            System.out.println("[IQ] AP Found: "+ apCount);
            if(isInitAp && apCount>=4){
                Location current = new Location(dimensionX/2f,dimensionY/2f);
                Location bottomLeft = new Location(0f,0f);
                Location topRight = new Location(dimensionX,dimensionY);
                Location latestCurrent = new Location(-1f,-1f);
                //System.out.println("[IQ] Start Calculation");
                System.out.println("[IQ] AP - {" + ap1.getStrength() + ", " + ap2.getStrength() + ", " + ap3.getStrength() + ", " + ap4.getStrength() + "}");
                while(true){
                    //System.out.println("[IQ] " + current.toString());
                    //System.out.println("[IQ] " + bottomLeft.toString() + " to " + topRight.toString());
                    int[] weight = CheckValid(current);
                    int maxCount=0;
                    for(int i=1;i<=9;i++)maxCount=Math.max(maxCount,weight[i]);
                    ArrayList<Location> bottomLeftSet = new ArrayList<>();
                    ArrayList<Location> topRightSet = new ArrayList<>();

                    boolean isAnyMax=false;

                    if(weight[1]==maxCount){
                        isAnyMax=true;
                        Location newbottomLeft=new Location(bottomLeft.x,bottomLeft.y);
                        Location newtopRight=new Location(current.x,current.y);
                        bottomLeftSet.add(newbottomLeft);
                        topRightSet.add(newtopRight);
                    }

                    if(weight[2]==maxCount){
                        isAnyMax=true;
                        Location newbottomLeft=new Location((bottomLeft.x+current.x)/2,bottomLeft.y);
                        Location newtopRight = new Location((current.x+topRight.x)/2,current.y);
                        bottomLeftSet.add(newbottomLeft);
                        topRightSet.add(newtopRight);
                    }

                    if(weight[3]==maxCount){
                        isAnyMax=true;
                        Location newbottomLeft=new Location(current.x,bottomLeft.y);
                        Location newtopRight = new Location(topRight.x,current.y);
                        bottomLeftSet.add(newbottomLeft);
                        topRightSet.add(newtopRight);
                    }

                    if(weight[4]==maxCount){
                        isAnyMax=true;
                        Location newbottomLeft=new Location(bottomLeft.x,(bottomLeft.y+current.y)/2);
                        Location newtopRight = new Location(current.x,(current.y+topRight.y)/2);
                        bottomLeftSet.add(newbottomLeft);
                        topRightSet.add(newtopRight);
                    }

                    if(weight[6]==maxCount){
                        isAnyMax=true;
                        Location newbottomLeft=new Location(current.x,(bottomLeft.y+current.y)/2);
                        Location newtopRight = new Location(topRight.x,(current.y+topRight.y)/2);
                        bottomLeftSet.add(newbottomLeft);
                        topRightSet.add(newtopRight);
                    }

                    if(weight[7]==maxCount){
                        isAnyMax=true;
                        Location newbottomLeft=new Location(bottomLeft.x,current.y);
                        Location newtopRight=new Location(current.x,topRight.y);
                        bottomLeftSet.add(newbottomLeft);
                        topRightSet.add(newtopRight);
                    }

                    if(weight[8]==maxCount){
                        isAnyMax=true;
                        Location newbottomLeft=new Location((bottomLeft.x+current.x)/2,current.y);
                        Location newtopRight = new Location((current.x+topRight.x)/2,topRight.y);
                        bottomLeftSet.add(newbottomLeft);
                        topRightSet.add(newtopRight);
                    }

                    if(weight[9]==maxCount){
                        isAnyMax=true;
                        Location newbottomLeft=new Location(current.x,current.y);
                        Location newtopRight=new Location(topRight.x,topRight.y);
                        bottomLeftSet.add(newbottomLeft);
                        topRightSet.add(newtopRight);
                    }

                    if(weight[5]==maxCount && !isAnyMax)break;
                    if(Location.Distance(current,latestCurrent)==0){
                        break;
                        /*if(weight[5]==maxCount)break;
                        ap1.randomStrength();
                        ap2.randomStrength();
                        ap3.randomStrength();
                        ap4.randomStrength();*/
                    }
                    latestCurrent = current.clone();

                    bottomLeft = new Location(dimensionX,dimensionY);
                    for(Location l : bottomLeftSet){
                        bottomLeft.x = Math.min(bottomLeft.x,l.x);
                        bottomLeft.y = Math.min(bottomLeft.y,l.y);
                    }
                    topRight = new Location(0f,0f);
                    for(Location l : topRightSet){
                        topRight.x = Math.max(topRight.x,l.x);
                        topRight.y = Math.max(topRight.y,l.y);
                    }
                    current=new Location((bottomLeft.x+topRight.x)/2,(bottomLeft.y+topRight.y)/2);
                }

                if(Location.Distance(current,expPosition)<=20) {
                    FirebaseDatabase.getInstance().getReference("/experiment/p" + (int) expPosition.x + "-" + (int) expPosition.y + "/" + count + "/x/").setValue(current.x);
                    FirebaseDatabase.getInstance().getReference("/experiment/p" + (int) expPosition.x + "-" + (int) expPosition.y + "/" + count + "/y/").setValue(current.y);
                    count++;
                }

                System.out.println("[IQ] Result is " + current.toString());
                String tvText = current.toString();
                tvText += "\ncount: "+count+" avgError: " + avgError;
                tvText += "\n" + ap3.toString() + "\t" + ap4.toString();
                tvText += "\n" + ap1.toString() + "\t" + ap2.toString();
                tvText += "\n" + "last update: " + new SimpleDateFormat("HH:mm:ss").format(new Date());
                tvLocation.setText(tvText);

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(-2,-2);
                lp.leftMargin = Math.max(1,Math.min(indoorBg.getWidth()-pointer.getWidth()-1,(int)(indoorBg.getWidth() * current.x / dimensionX) - pointer.getWidth()/2));
                lp.topMargin = Math.max(1,Math.min(indoorBg.getHeight()-pointer.getHeight()-1,(int)(indoorBg.getHeight() * (1-current.y / dimensionY) - pointer.getHeight()/2)));
                pointer.setLayoutParams(lp);
            }
            //else {
                wifiManager.startScan(); //start a new scan to update values faster
            //}
        }
    }
}

class APInfo{
    private String name;
    private ArrayList<Float> strength;

    public APInfo(String name) {
        this.name = name;
        strength=new ArrayList<>();
        strength.add(0f);
    }

    public APInfo(String name, float strength) {
        this.name = name;
        this.strength=new ArrayList<>();
        this.strength.add(strength);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getStrength() {
        /*float max=0;
        float min=999;
        float sum=0;
        for(float s : this.strength){
            sum+=s;
            min=Math.min(s,min);
            max=Math.max(s,max);
        }
        if(strength.size()>2){
            return (sum-min-max)/(this.strength.size()-2);
        }
        else{
            return sum/this.strength.size();
        }*/
        return this.strength.get(this.strength.size()-1);
    }

    public void setStrength(float strength) {
        //if(this.strength.get(this.strength.size()-1)==strength)return ;
        this.strength.add(strength);
        if(this.strength.size()>3)this.strength.remove(0);
    }

    @Override
    public String toString() {
        return name+"["+getStrength()+"]";
    }
}

class Location{
    public float x;
    public float y;

    public Location() {
        x=0f;
        y=0f;
    }

    public Location(float x, float y) {
        this.x = x;
        this.y = y;
    }
    public static double Distance(Location a, Location b){
        return Math.sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
    }

    @Override
    public String toString() {
        return "("+x+", "+y+")";
    }

    public Location clone(){
        return new Location(x,y);
    }
}

